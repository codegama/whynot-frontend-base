import React, { Component } from "react";
import AuthHeader from "./Header/AuthHeader";
import { Notify } from "react-redux-notify";
import LatestFooter from "./Footer/LatestFooter";
import ThemeFooter from "./Footer/ThemeFooter";

class AuthLayout extends Component {
  state = {};
  render() {
    return (
      <body>
        <Notify position="TopRight" />
        <div>
          {React.cloneElement(this.props.children)}
        </div>
        {/* <NewFooter /> */}
        {/* <LatestFooter /> */}
        <ThemeFooter />
      </body>
    );
  }
}

export default AuthLayout;
