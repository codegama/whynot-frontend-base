import React from "react";
import { Link, NavLink } from "react-router-dom";
import { Col, Image } from "react-bootstrap";
import { connect } from "react-redux";
import { fetchBookmarksStart } from "../../store/actions/BookmarkAction";
import { translate, t } from "react-multi-lang";

const BookmarkNav = (props) => {
  return (
    <Col xs={12} sm={12} md={3}>
      <div className="vertical-menu">
        <NavLink
          activeClassName="active"
          className="bookmarkes-list"
          to={"/bookmarks"}
        >
          <Link to={"/list"}>
            <Image
              src={window.location.origin + "/assets/images/icons/back.svg"}
              className="svg-clone"
            />
          </Link>
         <span> {t("bookmarks")}</span>
        </NavLink>

        <NavLink
          activeClassName="active"
          className="bookmarkes-list"
          to={"/bookmarks"}
        >
          <Image
            src="assets/images/icons/bookmark.svg"
            className="svg-clone my-p-icons"
          />
          <span>{t("all_bookmarks")}</span>
        </NavLink>
      </div>
    </Col>
  );
};

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(null, mapDispatchToProps)(translate(BookmarkNav));
