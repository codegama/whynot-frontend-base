import React from "react";
import {Form, Button, Image, Modal} from "react-bootstrap";
import { translate, t } from "react-multi-lang";

const AddFavModal = (props) => {
  return (
    <>
      <Modal
        className="modal-dialog-center"
        size="md"
        centered
        show={props.addFav}
        onHide={props.closeAddFavModal}
      >
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>{t("send_tip")}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="header-userinfo">
              <div className="g-avatar online_status_class">
                <Image
                  src="assets/images/avatar/user-4.jpg"
                  alt="💕🦋Sarai Rollins🦋💕"
                  className="tips__user__img"
                />
              </div>
              <div className="popup-username-row">
                <div className="pop-username">
                  <div className="">{t("sarai_rollins")}</div>
                </div>
              </div>
              <div className="popup-username-row">
                <span className="pop-username popuser-realname">
                  <div className="pop-user-username">{t("sarairollins")}</div>
                </span>
              </div>
            </div>

            <div className="floating-form">
              <div className="floating-label">
                <input className="floating-input" type="text" placeholder=" " />
                <span className="highlight"></span>
                <label className="default-label">{t("tip_amount")}</label>
              </div>

              <div className="floating-label">
                <input className="floating-input" type="text" placeholder=" " />
                <span className="highlight"></span>
                <label className="default-label">{t("message_optional")}</label>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              type="button"
              className="btn btn-danger"
              data-dismiss="modal"
            >
             {t("CANCEL")}
            </Button>
            <Button
              type="button"
              className="btn btn-success"
              data-dismiss="modal"
            >
              {t("send_tip")}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default (translate(AddFavModal));
