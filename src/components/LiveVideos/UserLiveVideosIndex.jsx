import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Container, Row, Col, Image } from "react-bootstrap";
import "../Wallet/Wallet.css";
import { fetchLiveVideosScheduledOwnerStart } from "../../store/actions/LiveVideoAction";
import { Link } from "react-router-dom";
import NoDataFound from "../NoDataFound/NoDataFound";
import BillingAccountLoader from "../Loader/BillingAccountLoader";
import { translate, t } from "react-multi-lang";
import GoLiveModal from "../helper/GoLiveModal";
import { fetchUserDetailsStart } from "../../store/actions/UserAction";
import LiveDataCard from "./LiveDataCard";

const UserLiveVideosIndex = (props) => {
  const [goLive, setGoLive] = useState(false);

  useEffect(() => {
    if (props.userDetails.loading) props.dispatch(fetchUserDetailsStart());
    props.dispatch(fetchLiveVideosScheduledOwnerStart());
  }, []);

  const closeGoLiveModal = () => {
    setGoLive(false);
  };
  console.log(props.liveVideos.data);
  return (
    <>
      <div className="wallet-sec live-video-list-header-sec">
        <Container>
          <Row>
            <Col sm={12} md={12}>
              <div className="wallet-header-sec">
                <Row>
                  <Col sm={12} md={12} xl={4}>
                    <Link
                      to={"/live-videos-history"}
                      className="bookmarkes-list notify-title back-button"
                      onClick={() => {
                        props.history.goBack();
                      }}
                    >
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/icons/back.svg"
                        }
                        className="svg-clone"
                      />
                      <h3 className="ml-2 mb-0">
                        {t("scheduled_live_videos")}
                      </h3>
                    </Link>
                  </Col>
                  <Col sm={12} md={12} xl={8} className="text-right">
                    <div>
                      <Link
                        className="live-history-btn mr-2"
                        to={"/live-videos-history"}
                      >
                        {t("live_history")}
                      </Link>

                      <Link to={"/go-live"} className="go-live-btn">
                        {t("go_live")}
                      </Link>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="trans-table-sec">
        <Container>
          {props.liveVideos.loading ? (
            t("loading")
          ) : props.liveVideos.data.videos.length > 0 ? (
            <div className="video-list-sec">
              {props.liveVideos.data.videos.map((video) => (
                <LiveDataCard video={video} key={video.live_video_id} />
              ))}
            </div>
          ) : (
            <NoDataFound />
          )}
        </Container>
        {props.userDetails.loading ? (
          t("loading")
        ) : (
          <GoLiveModal
            goLive={goLive}
            closeGoLiveModal={closeGoLiveModal}
            username={props.userDetails.data.username}
            userPicture={props.userDetails.data.picture}
            name={props.userDetails.data.name}
            user_id={props.userDetails.data.user_id}
          />
        )}
      </div>
    </>
  );
};

const mapStateToPros = (state) => ({
  liveVideos: state.liveVideo.liveVideosScheduledOwner,
  userDetails: state.users.profile,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(translate(UserLiveVideosIndex));
