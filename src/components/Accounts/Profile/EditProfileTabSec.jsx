import React from "react";
import { Link } from "react-router-dom";
import { Image, Media } from "react-bootstrap";
import { translate, t } from "react-multi-lang";
import configuration from "react-global-configuration";

const EditProfileTabSec = (props) => {
  return (
    <ul className="nav nav-tabs edit-profile-tabs" role="tablist">
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "settings-card" ? "active" : ""}
      >
        <Link
          to={"/"}
          className="bookmarkes-list"
          // aria-controls="profile"
          // role="tab"
          // data-toggle="tab"
          // onClick={() => props.setActiveSec("settings-card")}
        >
          <Image
            src={window.location.origin + "/assets/images/icons/back.svg"}
            className="svg-clone"
          />
          {t("settings")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "profile-card" ? "active" : ""}
      >
        <Link
          to="#Section2"
          aria-controls="profile"
          role="tab"
          className="bookmarkes-list"
          data-toggle="tab"
          onClick={() => props.setActiveSec("profile-card")}
        >
          <Image src="assets/images/icons/profile.svg" className="svg-clone" />
          {t("profile")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "change-password-card" ? "active" : ""}
      >
        <Link
          to="#Section3"
          aria-controls="profile"
          role="tab"
          className="bookmarkes-list"
          data-toggle="tab"
          onClick={() => props.setActiveSec("change-password-card")}
        >
          <Image src="assets/images/icons/change-1.svg" className="svg-clone" />
          {t("change_password")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "delete-account-card" ? "active" : ""}
      >
        <Link
          to="#Section4"
          aria-controls="profile"
          role="tab"
          className="bookmarkes-list"
          data-toggle="tab"
          onClick={() => props.setActiveSec("delete-account-card")}
        >
          <Image src="assets/images/icons/delete.png" className="svg-clone" />
          {t("delete_account")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "account-card" ? "active" : ""}
      >
        <Link
          to={{
            pathname: "/add-bank",
            state: {
              prevPath: props.location.pathname,
            },
          }}
          className="bookmarkes-list"
        >
          <Image src="assets/images/icons/account.svg" className="svg-clone" />
          {t("add_bank")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "notifications-card" ? "active" : ""}
      >
        <Link to={"/payments"} className="bookmarkes-list">
          <Image
            src="assets/images/icons/paper-money.png"
            className="svg-clone"
          />
          {t("payments")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "security-card" ? "active" : ""}
      >
        <Link to={`/document-upload`} className="bookmarkes-list">
          <Image
            src="assets/images/icons/documents.png"
            className="svg-clone"
          />
          {t("documents")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "bank-accounts" ? "active" : ""}
      >
        <Link to={`/billing-accounts`} className="bookmarkes-list">
          <Image
            src="assets/images/icons/merchant-account.png"
            className="svg-clone"
          />
          {t("bank_accounts")}
        </Link>
      </Media>
      <Media
        as="li"
        role="presentation"
        className={props.activeSec === "delivery-address" ? "active" : ""}
      >
        <Link to={`/delivery-address`} className="bookmarkes-list">
          <Image
            src="assets/images/icons/merchant-account.png"
            className="svg-clone"
          />
          {t("delivery_address")}
        </Link>
      </Media>
    </ul>
  );
};

export default translate(EditProfileTabSec);
