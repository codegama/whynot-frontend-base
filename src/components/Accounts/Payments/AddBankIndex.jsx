import React, { Component, useState } from "react";
import { connect } from "react-redux";
import { Button, Container, Row, Col } from "react-bootstrap";
import "./AddBankIndex.css";
import { addBankAccountStart } from "../../../store/actions/BankAccountAction";
import { translate, t } from "react-multi-lang";
import { Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const AddBankIndex = (props) => {
  const [agree, setAgree] = useState(false);

  const validationSchema = Yup.object().shape({
    route_number: Yup.string().required("Routing number is required *"),
    account_number: Yup.number()
      .required("Account number is required *")
      .min(0, "Invalid account number"),
    first_name: Yup.string().required("First name is required *"),
    last_name: Yup.string().required("Last name is required *"),
    bank_type: Yup.string().required("Bank type is required *"),
    business_name: Yup.string(),
  });

  const handleSubmit = (values) => {
    console.log(values);
    props.dispatch(addBankAccountStart(values));
  };

  return (
    <div className="card-list-sec">
      <Container>
        <Link
          className="bookmarkes-list notify-title back-button head-title"
          onClick={() =>
            props.location.state && props.location.state.prevPath
              ? props.history.goBack()
              : props.history.push("/")
          }
        >
          <img
            src={window.location.origin + "/assets/images/icons/back.svg"}
            className="svg-clone"
          />
          {t("add_bank")}
        </Link>
        {/* <h4 className="head-title">{t("add_bank")}</h4> */}
        <Row>
          <Col sm={12} md={12}>
            <div className="add-bank-box">
              <Formik
                initialValues={{
                  route_number: "",
                  account_number: "",
                  first_name: "",
                  last_name: "",
                  bank_type: "savings",
                  business_name: "",
                }}
                validationSchema={validationSchema}
                onSubmit={(values) => handleSubmit(values)}
              >
                {({ touched, errors, isSubmitting, setFieldValue }) => (
                  <Form noValidate>
                    <Col md={6}>
                      <div className="form-group">
                        <label>{t("routing_number")}: (*)</label>
                        <Field
                          className="form-control"
                          type="text"
                          placeholder={t("routing_number")}
                          name="route_number"
                        />
                        <ErrorMessage
                          component={"div"}
                          name="route_number"
                          className="text-danger text-right"
                        />
                      </div>
                    </Col>

                    <Col md={6}>
                      <div className="form-group">
                        <label>{t("account_number")}: (*)</label>
                        <Field
                          type="number"
                          className="form-control"
                          placeholder={t("account_number")}
                          name="account_number"
                        />
                        <ErrorMessage
                          component={"div"}
                          name="account_number"
                          className="text-danger text-right"
                        />
                      </div>
                    </Col>

                    <Col md={6}>
                      <div className="form-group">
                        <label>{t("first_name")}: (*)</label>
                        <Field
                          type="text"
                          className="form-control"
                          placeholder={t("first_name")}
                          name="first_name"
                        />
                        <ErrorMessage
                          component={"div"}
                          name="first_name"
                          className="text-danger text-right"
                        />
                      </div>
                    </Col>

                    <Col md={6}>
                      <div className="form-group">
                        <label>{t("last_name")}: (*)</label>
                        <Field
                          type="text"
                          className="form-control"
                          placeholder={t("last_name")}
                          name="last_name"
                        />
                        <ErrorMessage
                          component={"div"}
                          name="last_name"
                          className="text-danger text-right"
                        />
                      </div>
                    </Col>

                    <Col md={6}>
                      <div className="form-group">
                        <label>{t("type_of_bank")}: (*)</label>
                        <Field
                          as="select"
                          name="bank_type"
                          className="form-control"
                        >
                          <option value="savings">{t("savings")}</option>
                          <option value="checking">{t("checking")}</option>
                        </Field>
                        <ErrorMessage
                          component={"div"}
                          name="bank_type"
                          className="text-danger text-right"
                        />
                      </div>
                    </Col>

                    <Col md={6}>
                      <div className="form-group">
                        <label>
                          {t("business_name")}: ({t("optional")})
                        </label>
                        <Field
                          type="text"
                          className="form-control"
                          placeholder={t("business_name")}
                          name="business_name"
                        />
                        <ErrorMessage
                          component={"div"}
                          name="business_name"
                          className="text-danger text-right"
                        />
                      </div>
                    </Col>

                    <Col md={6}>
                      <div class="check-terms custom-control custom-checkbox custom-control-inline">
                        <input
                          type="checkbox"
                          id="customControlAutosizing"
                          class="custom-control-input"
                          checked={agree}
                          onChange={() => setAgree(!agree)}
                        />
                        <label
                          title=""
                          for="customControlAutosizing"
                          class="custom-control-label"
                        ></label>
                      </div>
                      <label>
                        {t("i_agree_to")}
                        <Link
                          target="_blank"
                          to="/page/terms"
                          className="hover-link"
                        >
                          {" "}
                          {t("terms_conditions")}{" "}
                        </Link>
                      </label>
                    </Col>

                    <div className="edit-save">
                      <Button
                        className="btn gradient-btn gradientcolor addBank"
                        type="submit"
                        disabled={props.bankAccount.buttonDisable || !agree}
                      >
                        {props.bankAccount.loadingButtonContent !== null
                          ? props.bankAccount.loadingButtonContent
                          : t("submit")}
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

const mapStateToPros = (state) => ({
  bankAccount: state.bankAccount.addBankAccountInput,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(translate(AddBankIndex));
