import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Container, Row, Col, Table, Badge } from "react-bootstrap";
import "../../Wallet/Wallet.css";
import {
    deleteDeliveryAddressStart,
    fetchDeliveryAddressStart,
    setDefaultDeliveryAddressStart,
} from "../../../store/actions/DeliveryAddressAction";
import { Link } from "react-router-dom";
import NoDataFound from "../../NoDataFound/NoDataFound";
import BillingAccountLoader from "../../Loader/BillingAccountLoader";
import { translate, t } from "react-multi-lang";

const DeliveryAddressIndex = (props) => {
  useEffect(() => {
    props.dispatch(fetchDeliveryAddressStart());
  }, []);
  return (
    <>
      <div className="wallet-sec">
        <Container>
          <Row>
            <Col sm={12} md={12}>
              <div className="wallet-header-sec">
                <Row>
                  <Col sm={12} md={12} xl={9}>
                    <Link
                      className="bookmarkes-list notify-title back-button"
                      onClick={() => props.history.goBack()}
                    >
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/icons/back.svg"
                        }
                        className="svg-clone"
                      />
                      <h3 className="ml-2 mb-0">{t("delivery_address")}</h3>
                    </Link>
                    {/* <p className="text-muted f-2">
                      {t("billing_accounts_para")}
                    </p> */}
                  </Col>
                  <Col sm={12} md={12} xl={3}>
                    <div className="edit-save">
                      <Link className="receive-btn-blue" to={"/add-delivery-address"}>
                        {t("add_delivery_address")}
                      </Link>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="trans-table-sec">
        <Container>
          {props.deliveryAddress.loading ? (
            <BillingAccountLoader />
          ) : props.deliveryAddress.data.delivery_addresses.length > 0 ? (
            <Row>
              <Col sm={12} md={12}>
                <div className="trans-table">
                  <Table borderedless responsive>
                    <thead>
                      <tr className="bg-white text-muted text-center">
                        <th>{t("name")}</th>
                        <th>{t("address")}</th>
                        <th>{t("pincode")}</th>
                        <th>{t("state")}</th>
                        <th>{t("landmark")}</th>
                        <th>{t("contact_number")}</th>
                        <th>{t("is_default")}</th>
                        <th>{t("status")}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {props.deliveryAddress.data.delivery_addresses.map(
                        (address) => (
                          <tr key={address.delivery_address_id}>
                            <td className="text-capitalize">
                              {address.name ? address.name : "-"}
                            </td>
                            <td className="text-capitalize">
                              {address.address ? address.address : "-"}
                            </td>
                            <td>
                              {address.pincode
                                ? address.pincode
                                : "-"}
                            </td>
                            <td className="text-capitalize">
                              {address.state ? address.state : "-"}
                            </td>
                            <td className="text-capitalize">
                              {address.landmark
                                ? address.landmark
                                : "-"}
                            </td>
                            <td className="amount">
                              {address.contact_number
                                ? address.contact_number
                                : "-"}
                            </td>
                            {address.is_default === 1 ? (
                              <td>
                                <Badge className="success-badge">
                                  {t("yes")}
                                </Badge>
                              </td>
                            ) : (
                              <td>
                                <Badge className="unconfirmed-badge">
                                  {t("no")}
                                </Badge>
                              </td>
                            )}
                            <td>
                              {address.is_default === 0 ? (
                                <Button
                                  variant="success"
                                  onClick={() =>
                                    props.dispatch(
                                        setDefaultDeliveryAddressStart({
                                            delivery_address_id:address.delivery_address_id,
                                        })
                                    )
                                  }
                                >
                                  {t("make_default")}
                                </Button>
                              ) : null}{" "}
                              <Button
                                variant="danger"
                                onClick={() => {
                                  if (
                                    window.confirm(
                                      t("delete_delivery_address_confirmation")
                                    )
                                  ) {
                                    props.dispatch(
                                        deleteDeliveryAddressStart({
                                            delivery_address_id:address.delivery_address_id,
                                      })
                                    );
                                  }
                                }}
                              >
                                {t("delete")}
                              </Button>{" "}
                            </td>
                          </tr>
                        )
                      )}
                    </tbody>
                  </Table>
                </div>
              </Col>
            </Row>
          ) : (
            <NoDataFound />
          )}
        </Container>
      </div>
    </>
  );
};

const mapStateToPros = (state) => ({
    deliveryAddress: state.deliveryAddress.deliveryAddress,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(translate(DeliveryAddressIndex));
